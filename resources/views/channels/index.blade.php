
<h1>Articles</h1>
<ul>

@if(count($dossiers)<=0)
<small>Aucun article</small>
@endif

@foreach ($dossiers as $dossier)
  <li><a href="{{ $dossier['url'] }}">{{ $dossier['titre'] }}</a></li>
@endforeach

</ul>
