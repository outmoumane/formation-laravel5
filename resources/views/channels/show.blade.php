
<h1>Articles</h1>
<ul>

@if(count($channel->articles())<=0)
<small>Aucun article</small>
@endif

@foreach ($channel->articles() as $article)
  <li>
    <h2>{{ $article->titre() }}</h2>
    <ul>
      <p><iframe src="{{ $article->video() }}" allowfullscreen="" frameborder="0"></iframe></p>
      <p><a href="{{ $article->url() }}">Lire+</a></p>
    </ul>
  </li>
@endforeach

</ul>
