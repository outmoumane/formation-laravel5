<?php

namespace App;

use \Mni\FrontYAML\Parser;
use Illuminate\Support\Facades\Storage;

class Article
{
    public $path;
    public $document;

    public function __construct($path)
    {
      abort_unless(Storage::exists($path),404);
      $this->path = $path;
      $this->document = (new Parser)->parse(Storage::get($path));
      //dd();
    }
    public function titre()
    {
      return $this->document->getYAML()['titre'];
    }
    public function video()
    {
      return $this->document->getYAML()['video'];
    }
    public function contenu()
    {
      return $this->document->getContent();
    }
    public function url()
    {
      return url(str_replace('.md','',$this->path));
    }
}
