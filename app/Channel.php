<?php

namespace App;

use Illuminate\Support\Facades\Storage;

class Channel
{
    public $slug;

    public function __construct($slug=null)
    {

      abort_unless(Storage::exists("articles/{$slug}"),404);
      $this->slug = $slug;
    }
    public function articles()
    {
      $articles = collect(Storage::allFiles("articles/{$this->slug}"))
        ->mapInto(Article::class);
      //dd($this, $articles);
      return $articles;
    }
}
