<?php

namespace App;

class Dossier
{
    public $path;

    public function __construct($path)
    {
      $this->path = $path;
      //dd();
    }
    public function titre()
    {
      return mb_strtoupper(str_replace('articles/','',$this->path));
    }
    public function url()
    {
      return url($this->path);
    }
}
