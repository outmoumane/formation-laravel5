<?php

namespace App\Http\Controllers;

use App\Channel;

class ChannelsController extends Controller
{
    public function show($slug=null)
    {
      $channel = new Channel($slug);
      //dd($channel);

      return view('channels.show',[
        'channel' => $channel,
      ]);
    }
}
