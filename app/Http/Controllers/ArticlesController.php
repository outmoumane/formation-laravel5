<?php

namespace App\Http\Controllers;

use App\Article;

class ArticlesController extends Controller
{
    public function show($channel,$slug)
    {
      $article = new Article("articles/$channel/$slug.md");

      return view('articles.show',[
        'article' => $article,
      ]);
    }
}
