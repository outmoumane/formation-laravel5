<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;

class ChannelsRootController extends Controller
{
    public function show()
    {
      $dossiers = Storage::allDirectories("articles");
      $temp = array();
      foreach ($dossiers as $dossier)
      {
        $temp[] = array(
          'titre' => mb_strtoupper(str_replace('articles/','',$dossier)),
          'url' => $dossier,
        );
      }
      //dd($dossiers);

      return view('channels.index',[
        'dossiers' => $temp,
      ]);
    }
}
