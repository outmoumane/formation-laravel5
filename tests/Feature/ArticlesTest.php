<?php

namespace Tests\Feature;

use Tests\TestCase;

class ArticlesTest extends TestCase
{
    /**
     *
     * @test
     */
    public function articleIsShown()
    {
      $this->withoutExceptionHandling();

/*
      $this->get('/')
        ->assertSuccessful()
        ->assertSee('Bonjour!');
*/
      $this->get('articles/decouverte')
          ->assertSuccessful()
          ->assertSee('<h1>Articles</h1>');
    }
}
